﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class MagRespawn : MonoBehaviour
{
    public GameObject mag;
    public XRSocketInteractor magSocket;

    void Start()
    {
        magSocket.onSelectExited.AddListener(spawnMag);
    }
    public void spawnMag(XRBaseInteractable interactable)
    {
        Invoke("createMag", 0f);
    }

    void createMag()
    {
        GameObject newMag = Instantiate(mag, transform.position, mag.transform.rotation);

        newMag.transform.parent = transform;
    }
}
