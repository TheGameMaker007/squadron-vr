﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class TeleportToSpawn : MonoBehaviour
{
    private Transform spawnPoint;

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        CharacterController player = GetComponent<CharacterController>();
        if(hit.gameObject.tag == "RedTeamSelect")
        {
            spawnPoint = hit.gameObject.GetComponent<Teleporter>().spawnPoint;
            int count = int.Parse(hit.gameObject.GetComponent<Teleporter>().text.text.ToString());
            count++;
            hit.gameObject.GetComponent<Teleporter>().text.text = count.ToString();
           // Debug.Log("GotHere");
            player.enabled = false;
            transform.position = spawnPoint.position;
            player.enabled = true;
        }
        else if (hit.gameObject.tag == "BlueTeamSelect")
        {
            spawnPoint = hit.gameObject.GetComponent<Teleporter>().spawnPoint;
            int count = int.Parse(hit.gameObject.GetComponent<Teleporter>().text.text.ToString());
            count++;
            hit.gameObject.GetComponent<Teleporter>().text.text = count.ToString();
            // Debug.Log("GotHere");
            player.enabled = false;
            transform.position = spawnPoint.position;
            player.enabled = true;
        }
        else if (hit.gameObject.tag == "DeathWall")
        {
            Debug.Log("GotHere");
            player.enabled = false;
            transform.position = spawnPoint.position;
            player.enabled = true;
        }
    }
}
